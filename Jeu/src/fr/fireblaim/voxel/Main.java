package fr.fireblaim.voxel;

import fr.fireblaim.voxel.graphics.Window;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

public class Main {

    private static String title = "Voxel";

    public Main() {

    }

    public void update() {
        if(Mouse.isGrabbed() && Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) Mouse.setGrabbed(false);
        if(!Mouse.isGrabbed() && Mouse.isButtonDown(0)) Mouse.setGrabbed(true);
        if(!Mouse.isGrabbed()) return;
    }

    public void render() {

    }

    public void renderGUI() {

    }

    public static void main(String[] args) {
        Main main = new Main();
        Window.createWindow(1280, 720, title, false);

        long before = System.nanoTime();
        double ns = 1000000000.0 / 60.0;
        long timer = System.currentTimeMillis();

        int frames = 0;
        int ticks = 0;

        while(!Display.isCloseRequested()) {
            long now = System.nanoTime();

            double elapsed = now - before;

            if(elapsed > ns) {
                main.update();
                ticks++;
                before += ns;
            } else {
                main.render();
                frames++;
            }

            if(System.currentTimeMillis() - timer > 1000) {
                Display.setTitle(title + " | " + frames + " fps, " + ticks + " ticks");
                frames = 0;
                ticks = 0;
                timer += 1000;
            }

            Display.update();
        }

        Display.destroy();
        System.exit(0);
    }

}