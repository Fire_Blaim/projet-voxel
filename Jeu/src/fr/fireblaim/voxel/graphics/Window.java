package fr.fireblaim.voxel.graphics;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

public class Window {

    public static void createWindow(int width,int height, String title, boolean isResizable) {
        try {
            Display.setDisplayMode(new DisplayMode(width, height));
            Display.setTitle(title);
            Display.setResizable(isResizable);
            Display.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
            System.out.println("Erreur : LWJGL non present ou non trouve");
        }
    }

}
